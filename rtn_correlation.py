#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 10 23:51:00 2018

@author: mahfuz
"""
import numpy as np

from scipy.stats import uniform,lognorm,poisson,expon

from rts_waveform import rts_waveform
from rts_time_constants import rts_time_constants

import matplotlib
import matplotlib.pyplot as plt
font = {'size' : 18}
matplotlib.rc('font', **font)
matplotlib.rcParams['lines.linewidth'] = 2

Vdd = 0.5
Vt = 0.3
q = 1.6e-19
T = 300

n_devices = 100

#v = rts_time_constants(Vg=0.5, Vt=0.3, T=300, xT=0.5e-9, ET0=q*0.85, Svt=0.020, Tox=1.3e-9)
#print(v)

N = 10000
Lambdah = 50
Lambdal = 100
Lambda_trap = 3
Lmu = 0
Lsigma = 1
delta_ary = np.zeros((2,n_devices))
n_trap_ary = poisson.rvs(mu=Lambda_trap, size=(n_devices))
waveform_ary = np.zeros((n_devices,N))
for i in range(n_devices):
    n_trap = n_trap_ary[i]
    print('Device', i, 'with', n_trap, 'number of traps.')
    q = np.zeros(N)
    for j in range(n_trap):
        tauh = expon.rvs(Lambdah)
        taul = expon.rvs(Lambdal)
        amp = lognorm.rvs(Lsigma, 0, np.exp(Lmu))
        print(tauh, taul, amp)
        q = q + amp * rts_waveform(N=N, Tauh=tauh, Taul=taul)
        
    waveform_ary[i] = q

"""
Plot waveforms
"""
if 1:   
    n_rows = 6
    n_columns = 5
    fig, axes = plt.subplots(n_rows, n_columns, figsize=(20,16))
    
    for i in range(n_rows):
        for j in range(n_columns):
            idx = int(np.floor(uniform.rvs() * n_devices))
            waveform = waveform_ary[idx]
            ax = axes[i,j]
            ax.plot(waveform)
            
    fig.tight_layout()

#ax.plot(waveform_ary[10])

"""
Plot amplitude correlation
"""
if 1:
    fig, ax = plt.subplots(figsize=(6,4))
    X = waveform_ary[:,0:int(N/100)].max(axis=1) - waveform_ary[:,0:int(N/100)].min(axis=1)
    Y = waveform_ary[:,int(N-N/100):N].max(axis=1) - waveform_ary[:,int(N-N/100):N].min(axis=1)
    corr = np.corrcoef(X, Y)
    print(corr)
    ax.plot(X, Y, 'o')