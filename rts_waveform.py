#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 17 00:04:11 2018

@author: mahfuz
"""

import numpy as np
from scipy.stats import expon

def rts_waveform (N, Tauh, Taul, verbose=False):
    if verbose: print('Input parameters:', N, Tauh, Taul)

    start = 1 if round(expon.rvs(scale=Tauh) / expon.rvs(scale=Taul)) > 0 else 0

    """
    create trigger points
    """
    idx = 0
    Trig = np.zeros(N)
    status = start
    count = 0
    while idx < N:
        if status == 0:
            e = int(np.round(expon.rvs(scale=Taul)))
        else:
            e = int(np.round(expon.rvs(scale=Tauh)))

        if e == 0:  # no transition, so cancel the previous trigger
            if Trig[idx] == 1:
                status = 0 if status == 1 else 1    # revert the status
                Trig[idx] = 0   # cancel trigger
        else:
            idx = idx + e
            if idx > N-1:
                break
            status = 0 if status == 1 else 1
            Trig[idx] = 1

        count += 1

    if verbose: print('Total loop count: ', count)

    """
    create waveform using trigger points
    """
    q = np.zeros(N)
    for ii in range(0, N):
        if ii == 0:
            prev = start
        else:
            prev = q[ii-1]

        if Trig[ii] == 0:
            q[ii] = prev
        else:
            q[ii] = round(np.logical_not(prev))

    return q.round()

if __name__ == "__main__":
    N = 10000
    q = rts_waveform(N, 10000, 5, verbose=True)
    n_high = q.sum()
    n_low = N - n_high
    r = n_high / n_low
    print('Ratio of High and Low: ', n_high, n_low, r, 1/r)
    t = range(N)
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(figsize=(10,4))
    ax.plot(t, q)

