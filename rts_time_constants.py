#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 23:03:15 2018

@author: mahfuz
"""
import numpy as np

def rts_time_constants (Vg=0.6, Vt=0.3, T=300, xT=1e-9, ET0=0, Tox=1.4e-9, Svt=0.015, L=60e-9, W=140e-9):
    """
    Physical constants
    """
    e = 2.7183
    k = 1.38e-23
    q = 1.6e-19
    h = 6.6e-34
    hR = h / 2 / np.pi
    m0 = 9.11e-31
    mn = 0.26 * m0 # for conductivity calculations
    # mn = 1.08 * m0 # for density of states calculations
    mp = 0.36 * m0 # for density of states calculations
    eps_0 = 8.854e-12
    epsr_Si = 11.7
    eps_Si = epsr_Si * eps_0
    epsr_ox = 3.9
    eps_ox = epsr_ox * eps_0
    # silicon electron affinity is 4.05 eV
    # chi_Si = 4.05 * q
    EgSi = 1.12 * q
    phi_0 = 3.1  # V
#    Nc = 2 * (2 * np.pi * mn * k * T / h**2)**(3/2)
#    Nv = 2 * (2 * np.pi * mp * k * T / h**2)**(3/2)
#    ni = np.sqrt(Nc * Nv) * np.exp(-EgSi / 2 / k / T)
    ni = 5.29e19 * (T / 300)**2.54 * np.exp(-6726/T)

    vth = np.sqrt(8 * k * T / mn)  # Thermal velocity: m/s


    # Na = 10**21 * (10**6) is for default bulk MOSFET
#    Na = 10**19 * (10**6)
    Na = (Svt * np.sqrt(L*W) / Tox / 3.2e-8)**(1/0.4) * 1e6
    Wdep = np.sqrt(4 * eps_Si * k * T * np.log(Na/ni) / q**2 / Na)
#    Svt_derived = q * Tox / eps_ox * np.sqrt(Na * Wdep / 4 / L / W)

    # Vc = 0     # reference point
    EcSi = 0   # reference point

    # EvSi = EcSi - EgSi
    E1 = 0.06 * q
    EiSi = EcSi - EgSi / 2

    # phi_p = 0  # poly gate depletion effect
    phi_b = k * T / q * np.log(Na / ni)
    EFSi = EiSi - q * phi_b
    EF = EFSi

    """
    Derived parameters
    """
    n0 = Na
    eta = 1.4
    ns = 2 * n0 * np.log(1 + 0.5 * np.exp(q*(Vg-Vt)/eta/k/T))
    Cox = eps_ox / Tox


    Vfb = Vt - 2 * phi_b - np.sqrt(Na * 2 * eps_Si * 2 * q * phi_b) / Cox

    # phi_s = phi_b + (2*k*T)./q .* log(Cox * (Vg - Vfb - 2*phi_b) / sqrt(2*eps_Si * k * T * Na))
    phi_s = k*T/q * np.log(ns * Na / ni**2)

    EcCh = EcSi - q * phi_s

    Vox = Vg - Vfb - phi_s

    ET = ET0 - q * phi_s - q * Vox * xT / Tox
    # ET = ET0

    E0 = EcCh - ET
    # E0 = ET - EcCh
    EcoxT = q*phi_0 - q*phi_s - q * Vox * xT / Tox
    ER = EcoxT - ET
    EB = (E0 - ER)**2 / 4 / ER

    sigma_0 = np.sqrt(E1 / ER) * (np.pi**2 / 2 / e) * (hR**2 / 2 / mn / k / T)
    Eoc = EB - k*T
    # Eoc = EB

    alpha_relax = np.exp(-Eoc / k / T)

    beta = hR / 2 / np.sqrt(2 * mn * (q*phi_0))

    alpha_tunnel = np.exp(-xT/beta)

    sigma = sigma_0 * alpha_tunnel * alpha_relax

    tau_ratio = np.exp((ET - EF) / k / T)

    tauc = 1 / ns / sigma / vth

    taue = tauc / tau_ratio

    return tauc, taue, EF, EcCh, EcoxT, ET, E0, ER, EB

#-------------------------------
if __name__ == "__main__":
    q = 1.6e-19
    v = rts_time_constants(Vg=0.5, Vt=0.3, T=300, xT=0.5e-9, ET0=q*0.85, Svt=0.020, Tox=1.3e-9)
    print(v)

